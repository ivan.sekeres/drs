/*****************************************************************************
* Filename:          D:\DRS\LV2_Sekeres_Vodicka\EDK/drivers/myled_ip_v1_00_a/src/myled_ip.c
* Version:           1.00.a
* Description:       myled_ip Driver Source File
* Date:              Thu Oct 26 06:39:20 2023 (by Create and Import Peripheral Wizard)
*****************************************************************************/


/***************************** Include Files *******************************/

#include "myled_ip.h"
#include "xparameters.h"

/************************** Function Definitions ***************************/

void SetLEDValue(int SwValue) 
{
	MYLED_IP_mWriteReg(XPAR_MYLED_IP_0_BASEADDR, 0, SwValue);
}