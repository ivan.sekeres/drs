/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xparameters.h"
#include "xgpio.h"

void print(char *str);

int main()
{
	init_platform();

	XGpio switches;
	XGpio leds;
	XGpio push;
	int switchesValue;
	int pushValue;

	XGpio_Initialize(&switches, XPAR_DIP_SWITCHES_8BITS_DEVICE_ID);
	XGpio_SetDataDirection(&switches, 1, 0xffffffff);

	XGpio_Initialize(&leds, XPAR_LED_DEVICE_ID);
	XGpio_SetDataDirection(&leds, 1, 0x00000000);

	XGpio_Initialize(&push, XPAR_PUSH_DEVICE_ID);
	XGpio_SetDataDirection(&push, 1, 0xffffffff);

	int i = 0;

	while(1)
	{
		switchesValue = XGpio_DiscreteRead(&switches, 1);
		pushValue = XGpio_DiscreteRead(&push, 1);

		//xil_printf("Stanje sklopki je 0x%x hex / %d dec\r\n",switchesValue, switchesValue);

		XGpio_DiscreteWrite(&leds, 1, switchesValue);

		if(pushValue == 1)
		{
			XGpio_DiscreteWrite(&leds, 1, 0b11110000);
		}

		if(pushValue == 2)
		{
			XGpio_DiscreteWrite(&leds, 1, 0b11111111);
		}

		if(pushValue == 4)
		{
			XGpio_DiscreteWrite(&leds, 1, 0b00001111);
		}

		if(pushValue == 8)
		{
			XGpio_DiscreteWrite(&leds, 1, 0b11110000);
			for(i = 0; i < 66666666/12; i++);
			XGpio_DiscreteWrite(&leds, 1, 0b00001111);
			for(i = 0; i < 66666666/12; i++);
		}
	}

	return 0;
}
