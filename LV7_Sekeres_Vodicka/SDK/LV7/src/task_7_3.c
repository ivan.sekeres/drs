#include <stdio.h>
#include "xparameters.h"
#include "xil_types.h"

void print(char *str);

int main()
{
	u8 data = 0, temp = 0;
	int i = 1, address;

	while(data != 13)
	{
		xil_printf("\nInput data: ");
		data = XUartLite_RecvByte(XPAR_UARTLITE_1_BASEADDR);

		address = XPAR_MICRON_RAM_MEM0_BASEADDR + i * 4;

		Xil_Out32LE(address, data);

		xil_printf("Primljeni podatak je: %d, a spremljen na adresu: %x\r\n", data, address);

		i++;
	}

	Xil_Out32LE(XPAR_MICRON_RAM_MEM0_BASEADDR, i - 1);

	while(1){
		xil_printf("\nInput rotation: ");
		data = XUartLite_RecvByte(XPAR_UARTLITE_1_BASEADDR);
		readAndRotation(data);
	}
    return 0;
}

void readAndRotation(u8 rotationDirection){
	int i = 1, address, j;
	int count = Xil_In32LE(XPAR_MICRON_RAM_MEM0_BASEADDR);
	u8 temp = 0, data[count];

	while(temp != 13){
		address = XPAR_MICRON_RAM_MEM0_BASEADDR + i * 4;
		data[i - 1] = Xil_In32LE(address);
		i++;
	}

	if(rotationDirection == 48){
		temp = data[0];
		for(j = 1; j < count - 1; j++){
			data[j] = data[j + 1];
		}
		data[count - 1] = temp;
	}
	else if(rotationDirection == 49){
		temp = data[count - 1];
		for(j = 1; j < count -1; j++){
			data[j] = data[j - 1];
		}
		data[0] = temp;
	}
	else{
		xil_printf("\nError!");
	}

	i = 1;

	while(temp != 13){
		address = XPAR_MICRON_RAM_MEM0_BASEADDR + i * 4;
		temp = Xil_In32LE(address);
		xil_printf("%d", temp);
		i++;
	}
}
