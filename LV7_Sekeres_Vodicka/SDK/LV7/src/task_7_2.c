#include <stdio.h>
#include "xparameters.h"
#include "xil_types.h"

void print(char *str);

int main()
{
	u8 data = 0, temp = 0;
	int i = 1, address;

	while(data != 13)
	{
		xil_printf("Input data: ");
		data = XUartLite_RecvByte(XPAR_UARTLITE_1_BASEADDR);

		address = XPAR_MICRON_RAM_MEM0_BASEADDR + i * 4;

		Xil_Out32LE(address, data);

		xil_printf("Primljeni podatak je: %d, a spremljen na adresu: %x\r\n", data, address);

		i++;
	}

	Xil_Out32LE(XPAR_MICRON_RAM_MEM0_BASEADDR, i - 1);

	i = 1;

	while(temp != 13){
		address = XPAR_MICRON_RAM_MEM0_BASEADDR + i * 4;
		temp = Xil_In32LE(address);
		xil_printf("\n\r%d", temp);
		i++;
	}

    return 0;
}
