library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity moving_light is
	--TO DO 1 -> deklarirati ulaz "clk" (STD_LOGIC) i izlaz "output" (STD_LOGIC_VECTOR) veli�ine 8 bita
    Port ( clk : in  STD_LOGIC;
         output : out  STD_LOGIC_VECTOR (7 downto 0)
		 );
end moving_light;

architecture Behavioral of moving_light is

	--TO DO 2 -> definirati korisni�ki tip podataka naziva "state" koji mo�e imati vrijednosti "state0", ... "state7"
	type state is (stanje0, stanje1, stanje2, stanje3, stanje4, stanje5, stanje6, stanje7);
	--TO DO 3 -> deklarirati signale "current_state" i "next_state" koji su tipa "state"
	signal current_state, next_state: state;
	--TO DO 4 -> deklarirati signal "clk_div" koji je tipa STD_LOGIC
	signal clk_div: std_logic;

begin

	--TO DO 5 -> instancirati generi�ki djelitelj frekvencije na na�in da od ulaznog singala takta kreira signal takta frekvencije 1 Hz
	divider_1Hz	:	entity work.generic_divider generic map (50000000) port map (clk, clk_div);
	
	----Lower section of FSM----
	process(clk_div)
	begin
		--TO DO 6 -> na rastu�i brid signala takta "clk_div" signalu "current_state" pridru�iti vrijednost signala "next_state"
		if(clk_div'event AND clk_div ='1') then
			current_state <= next_state;
		end if;
	end process;
	
	----Upper section of FSM----
	process(current_state)
	begin
		case current_state is
			--TO DO 7 -> u ovisnosti o vrijednosti signala "current_state" signalima "output" i "next_state" pridru�iti odgovaraju�e vrijednosti
			when  stanje0 => 
				output <= "00000001";
				next_state <= stanje1;
			when  stanje1 => 
				output <= "00000010";
				next_state <= stanje2;
			when   stanje2 =>
				output <= "00000100";
				next_state <= stanje3;
			when  stanje3 => 
				output <= "00001000";
				next_state <= stanje4;
			when  stanje4 => 
				output <= "00010000";
				next_state <= stanje5;
			when  stanje5 => 
				output <= "00100000";
				next_state <= stanje6;
			when  stanje6 => 
				output <= "01000000";
				next_state <= stanje7;
			when  stanje7 => 
				output <= "10000000";
				next_state <= stanje0;
		end case;
	end process;	

end Behavioral;

