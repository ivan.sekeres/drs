--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: traffic_light_CNTRL_synthesis.vhd
-- /___/   /\     Timestamp: Thu Nov 16 08:06:50 2023
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm traffic_light_CNTRL -w -dir netgen/synthesis -ofmt vhdl -sim traffic_light_CNTRL.ngc traffic_light_CNTRL_synthesis.vhd 
-- Device	: xc6slx16-3-csg324
-- Input file	: traffic_light_CNTRL.ngc
-- Output file	: D:\DRS\LV3_Sekeres_Vodicka\ISE\LV3_vhdl\netgen\synthesis\traffic_light_CNTRL_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: traffic_light_CNTRL
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity traffic_light_CNTRL is
  port (
    clk : in STD_LOGIC := 'X'; 
    segment : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    display : out STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end traffic_light_CNTRL;

architecture Structure of traffic_light_CNTRL is
  signal clk_BUFGP_0 : STD_LOGIC; 
  signal divider_1Hz_clk_t_1 : STD_LOGIC; 
  signal current_state_FSM_FFd1_2 : STD_LOGIC; 
  signal segment_4_OBUF_3 : STD_LOGIC; 
  signal segment_1_OBUF_4 : STD_LOGIC; 
  signal display_0_OBUF_5 : STD_LOGIC; 
  signal display_3_OBUF_6 : STD_LOGIC; 
  signal divider_1Hz_n0001_inv : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_7_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_8_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_9_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_10_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_11_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_12_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_13_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_14_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_15_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_16_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_17_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_18_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_19_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_20_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_21_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_22_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_23_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_24_Q : STD_LOGIC; 
  signal divider_1Hz_temp_25_GND_4_o_add_0_OUT_25_Q : STD_LOGIC; 
  signal n0002_inv : STD_LOGIC; 
  signal Result_0_1 : STD_LOGIC; 
  signal Result_1_1_58 : STD_LOGIC; 
  signal Result_2_1_59 : STD_LOGIC; 
  signal current_state_FSM_FFd2_In : STD_LOGIC; 
  signal current_state_FSM_FFd1_In : STD_LOGIC; 
  signal current_state_FSM_FFd2_85 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_lut_0_Q : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_0_Q_116 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_1_Q_117 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_2_Q_118 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_3_Q_119 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_4_Q_120 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_5_Q_121 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_6_Q_122 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_7_Q_123 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_8_Q_124 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_9_Q_125 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_10_Q_126 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_11_Q_127 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_12_Q_128 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_13_Q_129 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_14_Q_130 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_15_Q_131 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_16_Q_132 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_17_Q_133 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_18_Q_134 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_19_Q_135 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_20_Q_136 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_21_Q_137 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_22_Q_138 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_23_Q_139 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_24_Q_140 : STD_LOGIC; 
  signal divider_1Hz_n0001_inv1_141 : STD_LOGIC; 
  signal divider_1Hz_n0001_inv2_142 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_1_rt_156 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_2_rt_157 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_3_rt_158 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_4_rt_159 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_5_rt_160 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_6_rt_161 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_7_rt_162 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_8_rt_163 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_9_rt_164 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_10_rt_165 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_11_rt_166 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_12_rt_167 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_13_rt_168 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_14_rt_169 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_15_rt_170 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_16_rt_171 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_17_rt_172 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_18_rt_173 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_19_rt_174 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_20_rt_175 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_21_rt_176 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_22_rt_177 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_23_rt_178 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_cy_24_rt_179 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_1_rt_180 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_2_rt_181 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_3_rt_182 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_4_rt_183 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_5_rt_184 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_6_rt_185 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_7_rt_186 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_8_rt_187 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_9_rt_188 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_10_rt_189 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_11_rt_190 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_12_rt_191 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_13_rt_192 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_14_rt_193 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_15_rt_194 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_16_rt_195 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_17_rt_196 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_18_rt_197 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_19_rt_198 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_20_rt_199 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_21_rt_200 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_22_rt_201 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_23_rt_202 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_24_rt_203 : STD_LOGIC; 
  signal divider_1Hz_Mcount_temp_xor_25_rt_204 : STD_LOGIC; 
  signal divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_25_rt_205 : STD_LOGIC; 
  signal divider_1Hz_clk_t_dpot_206 : STD_LOGIC; 
  signal divider_1Hz_temp_0_rstpot_207 : STD_LOGIC; 
  signal divider_1Hz_temp_1_rstpot_208 : STD_LOGIC; 
  signal divider_1Hz_temp_2_rstpot_209 : STD_LOGIC; 
  signal divider_1Hz_temp_3_rstpot_210 : STD_LOGIC; 
  signal divider_1Hz_temp_4_rstpot_211 : STD_LOGIC; 
  signal divider_1Hz_temp_5_rstpot_212 : STD_LOGIC; 
  signal divider_1Hz_temp_6_rstpot_213 : STD_LOGIC; 
  signal divider_1Hz_temp_7_rstpot_214 : STD_LOGIC; 
  signal divider_1Hz_temp_8_rstpot_215 : STD_LOGIC; 
  signal divider_1Hz_temp_9_rstpot_216 : STD_LOGIC; 
  signal divider_1Hz_temp_10_rstpot_217 : STD_LOGIC; 
  signal divider_1Hz_temp_11_rstpot_218 : STD_LOGIC; 
  signal divider_1Hz_temp_12_rstpot_219 : STD_LOGIC; 
  signal divider_1Hz_temp_13_rstpot_220 : STD_LOGIC; 
  signal divider_1Hz_temp_14_rstpot_221 : STD_LOGIC; 
  signal divider_1Hz_temp_15_rstpot_222 : STD_LOGIC; 
  signal divider_1Hz_temp_16_rstpot_223 : STD_LOGIC; 
  signal divider_1Hz_temp_17_rstpot_224 : STD_LOGIC; 
  signal divider_1Hz_temp_18_rstpot_225 : STD_LOGIC; 
  signal divider_1Hz_temp_19_rstpot_226 : STD_LOGIC; 
  signal divider_1Hz_temp_20_rstpot_227 : STD_LOGIC; 
  signal divider_1Hz_temp_21_rstpot_228 : STD_LOGIC; 
  signal divider_1Hz_temp_22_rstpot_229 : STD_LOGIC; 
  signal divider_1Hz_temp_23_rstpot_230 : STD_LOGIC; 
  signal divider_1Hz_temp_24_rstpot_231 : STD_LOGIC; 
  signal divider_1Hz_temp_25_rstpot_232 : STD_LOGIC; 
  signal N6 : STD_LOGIC; 
  signal N7 : STD_LOGIC; 
  signal N12 : STD_LOGIC; 
  signal N14 : STD_LOGIC; 
  signal N16 : STD_LOGIC; 
  signal divider_1Hz_n0001_inv4_238 : STD_LOGIC; 
  signal divider_1Hz_temp : STD_LOGIC_VECTOR ( 25 downto 0 ); 
  signal Result : STD_LOGIC_VECTOR ( 25 downto 0 ); 
  signal counter : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal divider_1Hz_Mcount_temp_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal divider_1Hz_Mcount_temp_cy : STD_LOGIC_VECTOR ( 24 downto 0 ); 
begin
  XST_VCC : VCC
    port map (
      P => display_0_OBUF_5
    );
  XST_GND : GND
    port map (
      G => display_3_OBUF_6
    );
  divider_1Hz_clk_t : FDE
    port map (
      C => clk_BUFGP_0,
      CE => divider_1Hz_temp_25_GND_4_o_add_0_OUT_25_Q,
      D => divider_1Hz_clk_t_dpot_206,
      Q => divider_1Hz_clk_t_1
    );
  current_state_FSM_FFd2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => divider_1Hz_clk_t_1,
      D => current_state_FSM_FFd2_In,
      Q => current_state_FSM_FFd2_85
    );
  current_state_FSM_FFd1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => divider_1Hz_clk_t_1,
      D => current_state_FSM_FFd1_In,
      Q => current_state_FSM_FFd1_2
    );
  counter_0 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => divider_1Hz_clk_t_1,
      D => Result(0),
      R => n0002_inv,
      Q => counter(0)
    );
  counter_1 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => divider_1Hz_clk_t_1,
      D => Result(1),
      R => n0002_inv,
      Q => counter(1)
    );
  counter_2 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => divider_1Hz_clk_t_1,
      D => Result(2),
      R => n0002_inv,
      Q => counter(2)
    );
  divider_1Hz_Mcount_temp_cy_0_Q : MUXCY
    port map (
      CI => display_3_OBUF_6,
      DI => display_0_OBUF_5,
      S => divider_1Hz_Mcount_temp_lut(0),
      O => divider_1Hz_Mcount_temp_cy(0)
    );
  divider_1Hz_Mcount_temp_xor_0_Q : XORCY
    port map (
      CI => display_3_OBUF_6,
      LI => divider_1Hz_Mcount_temp_lut(0),
      O => Result_0_1
    );
  divider_1Hz_Mcount_temp_cy_1_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(0),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_1_rt_156,
      O => divider_1Hz_Mcount_temp_cy(1)
    );
  divider_1Hz_Mcount_temp_xor_1_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(0),
      LI => divider_1Hz_Mcount_temp_cy_1_rt_156,
      O => Result_1_1_58
    );
  divider_1Hz_Mcount_temp_cy_2_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(1),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_2_rt_157,
      O => divider_1Hz_Mcount_temp_cy(2)
    );
  divider_1Hz_Mcount_temp_xor_2_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(1),
      LI => divider_1Hz_Mcount_temp_cy_2_rt_157,
      O => Result_2_1_59
    );
  divider_1Hz_Mcount_temp_cy_3_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(2),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_3_rt_158,
      O => divider_1Hz_Mcount_temp_cy(3)
    );
  divider_1Hz_Mcount_temp_xor_3_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(2),
      LI => divider_1Hz_Mcount_temp_cy_3_rt_158,
      O => Result(3)
    );
  divider_1Hz_Mcount_temp_cy_4_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(3),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_4_rt_159,
      O => divider_1Hz_Mcount_temp_cy(4)
    );
  divider_1Hz_Mcount_temp_xor_4_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(3),
      LI => divider_1Hz_Mcount_temp_cy_4_rt_159,
      O => Result(4)
    );
  divider_1Hz_Mcount_temp_cy_5_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(4),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_5_rt_160,
      O => divider_1Hz_Mcount_temp_cy(5)
    );
  divider_1Hz_Mcount_temp_xor_5_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(4),
      LI => divider_1Hz_Mcount_temp_cy_5_rt_160,
      O => Result(5)
    );
  divider_1Hz_Mcount_temp_cy_6_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(5),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_6_rt_161,
      O => divider_1Hz_Mcount_temp_cy(6)
    );
  divider_1Hz_Mcount_temp_xor_6_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(5),
      LI => divider_1Hz_Mcount_temp_cy_6_rt_161,
      O => Result(6)
    );
  divider_1Hz_Mcount_temp_cy_7_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(6),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_7_rt_162,
      O => divider_1Hz_Mcount_temp_cy(7)
    );
  divider_1Hz_Mcount_temp_xor_7_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(6),
      LI => divider_1Hz_Mcount_temp_cy_7_rt_162,
      O => Result(7)
    );
  divider_1Hz_Mcount_temp_cy_8_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(7),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_8_rt_163,
      O => divider_1Hz_Mcount_temp_cy(8)
    );
  divider_1Hz_Mcount_temp_xor_8_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(7),
      LI => divider_1Hz_Mcount_temp_cy_8_rt_163,
      O => Result(8)
    );
  divider_1Hz_Mcount_temp_cy_9_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(8),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_9_rt_164,
      O => divider_1Hz_Mcount_temp_cy(9)
    );
  divider_1Hz_Mcount_temp_xor_9_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(8),
      LI => divider_1Hz_Mcount_temp_cy_9_rt_164,
      O => Result(9)
    );
  divider_1Hz_Mcount_temp_cy_10_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(9),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_10_rt_165,
      O => divider_1Hz_Mcount_temp_cy(10)
    );
  divider_1Hz_Mcount_temp_xor_10_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(9),
      LI => divider_1Hz_Mcount_temp_cy_10_rt_165,
      O => Result(10)
    );
  divider_1Hz_Mcount_temp_cy_11_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(10),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_11_rt_166,
      O => divider_1Hz_Mcount_temp_cy(11)
    );
  divider_1Hz_Mcount_temp_xor_11_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(10),
      LI => divider_1Hz_Mcount_temp_cy_11_rt_166,
      O => Result(11)
    );
  divider_1Hz_Mcount_temp_cy_12_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(11),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_12_rt_167,
      O => divider_1Hz_Mcount_temp_cy(12)
    );
  divider_1Hz_Mcount_temp_xor_12_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(11),
      LI => divider_1Hz_Mcount_temp_cy_12_rt_167,
      O => Result(12)
    );
  divider_1Hz_Mcount_temp_cy_13_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(12),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_13_rt_168,
      O => divider_1Hz_Mcount_temp_cy(13)
    );
  divider_1Hz_Mcount_temp_xor_13_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(12),
      LI => divider_1Hz_Mcount_temp_cy_13_rt_168,
      O => Result(13)
    );
  divider_1Hz_Mcount_temp_cy_14_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(13),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_14_rt_169,
      O => divider_1Hz_Mcount_temp_cy(14)
    );
  divider_1Hz_Mcount_temp_xor_14_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(13),
      LI => divider_1Hz_Mcount_temp_cy_14_rt_169,
      O => Result(14)
    );
  divider_1Hz_Mcount_temp_cy_15_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(14),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_15_rt_170,
      O => divider_1Hz_Mcount_temp_cy(15)
    );
  divider_1Hz_Mcount_temp_xor_15_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(14),
      LI => divider_1Hz_Mcount_temp_cy_15_rt_170,
      O => Result(15)
    );
  divider_1Hz_Mcount_temp_cy_16_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(15),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_16_rt_171,
      O => divider_1Hz_Mcount_temp_cy(16)
    );
  divider_1Hz_Mcount_temp_xor_16_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(15),
      LI => divider_1Hz_Mcount_temp_cy_16_rt_171,
      O => Result(16)
    );
  divider_1Hz_Mcount_temp_cy_17_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(16),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_17_rt_172,
      O => divider_1Hz_Mcount_temp_cy(17)
    );
  divider_1Hz_Mcount_temp_xor_17_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(16),
      LI => divider_1Hz_Mcount_temp_cy_17_rt_172,
      O => Result(17)
    );
  divider_1Hz_Mcount_temp_cy_18_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(17),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_18_rt_173,
      O => divider_1Hz_Mcount_temp_cy(18)
    );
  divider_1Hz_Mcount_temp_xor_18_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(17),
      LI => divider_1Hz_Mcount_temp_cy_18_rt_173,
      O => Result(18)
    );
  divider_1Hz_Mcount_temp_cy_19_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(18),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_19_rt_174,
      O => divider_1Hz_Mcount_temp_cy(19)
    );
  divider_1Hz_Mcount_temp_xor_19_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(18),
      LI => divider_1Hz_Mcount_temp_cy_19_rt_174,
      O => Result(19)
    );
  divider_1Hz_Mcount_temp_cy_20_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(19),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_20_rt_175,
      O => divider_1Hz_Mcount_temp_cy(20)
    );
  divider_1Hz_Mcount_temp_xor_20_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(19),
      LI => divider_1Hz_Mcount_temp_cy_20_rt_175,
      O => Result(20)
    );
  divider_1Hz_Mcount_temp_cy_21_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(20),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_21_rt_176,
      O => divider_1Hz_Mcount_temp_cy(21)
    );
  divider_1Hz_Mcount_temp_xor_21_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(20),
      LI => divider_1Hz_Mcount_temp_cy_21_rt_176,
      O => Result(21)
    );
  divider_1Hz_Mcount_temp_cy_22_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(21),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_22_rt_177,
      O => divider_1Hz_Mcount_temp_cy(22)
    );
  divider_1Hz_Mcount_temp_xor_22_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(21),
      LI => divider_1Hz_Mcount_temp_cy_22_rt_177,
      O => Result(22)
    );
  divider_1Hz_Mcount_temp_cy_23_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(22),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_23_rt_178,
      O => divider_1Hz_Mcount_temp_cy(23)
    );
  divider_1Hz_Mcount_temp_xor_23_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(22),
      LI => divider_1Hz_Mcount_temp_cy_23_rt_178,
      O => Result(23)
    );
  divider_1Hz_Mcount_temp_cy_24_Q : MUXCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(23),
      DI => display_3_OBUF_6,
      S => divider_1Hz_Mcount_temp_cy_24_rt_179,
      O => divider_1Hz_Mcount_temp_cy(24)
    );
  divider_1Hz_Mcount_temp_xor_24_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(23),
      LI => divider_1Hz_Mcount_temp_cy_24_rt_179,
      O => Result(24)
    );
  divider_1Hz_Mcount_temp_xor_25_Q : XORCY
    port map (
      CI => divider_1Hz_Mcount_temp_cy(24),
      LI => divider_1Hz_Mcount_temp_xor_25_rt_204,
      O => Result(25)
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_0_Q : MUXCY
    port map (
      CI => display_3_OBUF_6,
      DI => display_0_OBUF_5,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_lut_0_Q,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_0_Q_116
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_1_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_0_Q_116,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_1_rt_180,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_1_Q_117
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_2_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_1_Q_117,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_2_rt_181,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_2_Q_118
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_3_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_2_Q_118,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_3_rt_182,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_3_Q_119
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_4_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_3_Q_119,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_4_rt_183,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_4_Q_120
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_5_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_4_Q_120,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_5_rt_184,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_5_Q_121
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_6_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_5_Q_121,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_6_rt_185,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_6_Q_122
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_7_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_6_Q_122,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_7_rt_186,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_7_Q_123
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_7_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_6_Q_122,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_7_rt_186,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_7_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_8_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_7_Q_123,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_8_rt_187,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_8_Q_124
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_8_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_7_Q_123,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_8_rt_187,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_8_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_9_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_8_Q_124,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_9_rt_188,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_9_Q_125
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_9_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_8_Q_124,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_9_rt_188,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_9_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_10_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_9_Q_125,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_10_rt_189,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_10_Q_126
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_10_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_9_Q_125,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_10_rt_189,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_10_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_11_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_10_Q_126,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_11_rt_190,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_11_Q_127
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_11_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_10_Q_126,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_11_rt_190,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_11_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_12_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_11_Q_127,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_12_rt_191,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_12_Q_128
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_12_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_11_Q_127,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_12_rt_191,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_12_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_13_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_12_Q_128,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_13_rt_192,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_13_Q_129
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_13_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_12_Q_128,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_13_rt_192,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_13_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_14_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_13_Q_129,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_14_rt_193,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_14_Q_130
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_14_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_13_Q_129,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_14_rt_193,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_14_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_15_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_14_Q_130,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_15_rt_194,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_15_Q_131
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_15_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_14_Q_130,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_15_rt_194,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_15_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_16_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_15_Q_131,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_16_rt_195,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_16_Q_132
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_16_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_15_Q_131,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_16_rt_195,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_16_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_17_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_16_Q_132,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_17_rt_196,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_17_Q_133
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_17_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_16_Q_132,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_17_rt_196,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_17_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_18_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_17_Q_133,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_18_rt_197,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_18_Q_134
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_18_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_17_Q_133,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_18_rt_197,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_18_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_19_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_18_Q_134,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_19_rt_198,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_19_Q_135
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_19_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_18_Q_134,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_19_rt_198,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_19_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_20_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_19_Q_135,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_20_rt_199,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_20_Q_136
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_20_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_19_Q_135,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_20_rt_199,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_20_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_21_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_20_Q_136,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_21_rt_200,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_21_Q_137
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_21_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_20_Q_136,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_21_rt_200,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_21_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_22_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_21_Q_137,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_22_rt_201,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_22_Q_138
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_22_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_21_Q_137,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_22_rt_201,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_22_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_23_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_22_Q_138,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_23_rt_202,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_23_Q_139
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_23_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_22_Q_138,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_23_rt_202,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_23_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_24_Q : MUXCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_23_Q_139,
      DI => display_3_OBUF_6,
      S => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_24_rt_203,
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_24_Q_140
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_24_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_23_Q_139,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_24_rt_203,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_24_Q
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_25_Q : XORCY
    port map (
      CI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_24_Q_140,
      LI => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_25_rt_205,
      O => divider_1Hz_temp_25_GND_4_o_add_0_OUT_25_Q
    );
  current_state_n0026_4_1 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => current_state_FSM_FFd1_2,
      I1 => current_state_FSM_FFd2_85,
      O => segment_4_OBUF_3
    );
  Result_1_1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => counter(1),
      I1 => counter(0),
      O => Result(1)
    );
  Result_2_1 : LUT3
    generic map(
      INIT => X"6A"
    )
    port map (
      I0 => counter(2),
      I1 => counter(0),
      I2 => counter(1),
      O => Result(2)
    );
  divider_1Hz_n0001_inv1 : LUT5
    generic map(
      INIT => X"80000000"
    )
    port map (
      I0 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_19_Q,
      I1 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_20_Q,
      I2 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_21_Q,
      I3 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_22_Q,
      I4 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_23_Q,
      O => divider_1Hz_n0001_inv1_141
    );
  divider_1Hz_n0001_inv2 : LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
    port map (
      I0 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_7_Q,
      I1 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_8_Q,
      I2 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_9_Q,
      I3 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_10_Q,
      I4 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_11_Q,
      O => divider_1Hz_n0001_inv2_142
    );
  segment_7_OBUF : OBUF
    port map (
      I => current_state_FSM_FFd1_2,
      O => segment(7)
    );
  segment_6_OBUF : OBUF
    port map (
      I => display_0_OBUF_5,
      O => segment(6)
    );
  segment_5_OBUF : OBUF
    port map (
      I => display_0_OBUF_5,
      O => segment(5)
    );
  segment_4_OBUF : OBUF
    port map (
      I => segment_4_OBUF_3,
      O => segment(4)
    );
  segment_3_OBUF : OBUF
    port map (
      I => display_0_OBUF_5,
      O => segment(3)
    );
  segment_2_OBUF : OBUF
    port map (
      I => display_0_OBUF_5,
      O => segment(2)
    );
  segment_1_OBUF : OBUF
    port map (
      I => segment_1_OBUF_4,
      O => segment(1)
    );
  segment_0_OBUF : OBUF
    port map (
      I => display_0_OBUF_5,
      O => segment(0)
    );
  display_3_OBUF : OBUF
    port map (
      I => display_3_OBUF_6,
      O => display(3)
    );
  display_2_OBUF : OBUF
    port map (
      I => display_0_OBUF_5,
      O => display(2)
    );
  display_1_OBUF : OBUF
    port map (
      I => display_0_OBUF_5,
      O => display(1)
    );
  display_0_OBUF : OBUF
    port map (
      I => display_0_OBUF_5,
      O => display(0)
    );
  divider_1Hz_Mcount_temp_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(1),
      O => divider_1Hz_Mcount_temp_cy_1_rt_156
    );
  divider_1Hz_Mcount_temp_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(2),
      O => divider_1Hz_Mcount_temp_cy_2_rt_157
    );
  divider_1Hz_Mcount_temp_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(3),
      O => divider_1Hz_Mcount_temp_cy_3_rt_158
    );
  divider_1Hz_Mcount_temp_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(4),
      O => divider_1Hz_Mcount_temp_cy_4_rt_159
    );
  divider_1Hz_Mcount_temp_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(5),
      O => divider_1Hz_Mcount_temp_cy_5_rt_160
    );
  divider_1Hz_Mcount_temp_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(6),
      O => divider_1Hz_Mcount_temp_cy_6_rt_161
    );
  divider_1Hz_Mcount_temp_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(7),
      O => divider_1Hz_Mcount_temp_cy_7_rt_162
    );
  divider_1Hz_Mcount_temp_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(8),
      O => divider_1Hz_Mcount_temp_cy_8_rt_163
    );
  divider_1Hz_Mcount_temp_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(9),
      O => divider_1Hz_Mcount_temp_cy_9_rt_164
    );
  divider_1Hz_Mcount_temp_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(10),
      O => divider_1Hz_Mcount_temp_cy_10_rt_165
    );
  divider_1Hz_Mcount_temp_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(11),
      O => divider_1Hz_Mcount_temp_cy_11_rt_166
    );
  divider_1Hz_Mcount_temp_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(12),
      O => divider_1Hz_Mcount_temp_cy_12_rt_167
    );
  divider_1Hz_Mcount_temp_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(13),
      O => divider_1Hz_Mcount_temp_cy_13_rt_168
    );
  divider_1Hz_Mcount_temp_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(14),
      O => divider_1Hz_Mcount_temp_cy_14_rt_169
    );
  divider_1Hz_Mcount_temp_cy_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(15),
      O => divider_1Hz_Mcount_temp_cy_15_rt_170
    );
  divider_1Hz_Mcount_temp_cy_16_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(16),
      O => divider_1Hz_Mcount_temp_cy_16_rt_171
    );
  divider_1Hz_Mcount_temp_cy_17_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(17),
      O => divider_1Hz_Mcount_temp_cy_17_rt_172
    );
  divider_1Hz_Mcount_temp_cy_18_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(18),
      O => divider_1Hz_Mcount_temp_cy_18_rt_173
    );
  divider_1Hz_Mcount_temp_cy_19_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(19),
      O => divider_1Hz_Mcount_temp_cy_19_rt_174
    );
  divider_1Hz_Mcount_temp_cy_20_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(20),
      O => divider_1Hz_Mcount_temp_cy_20_rt_175
    );
  divider_1Hz_Mcount_temp_cy_21_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(21),
      O => divider_1Hz_Mcount_temp_cy_21_rt_176
    );
  divider_1Hz_Mcount_temp_cy_22_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(22),
      O => divider_1Hz_Mcount_temp_cy_22_rt_177
    );
  divider_1Hz_Mcount_temp_cy_23_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(23),
      O => divider_1Hz_Mcount_temp_cy_23_rt_178
    );
  divider_1Hz_Mcount_temp_cy_24_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(24),
      O => divider_1Hz_Mcount_temp_cy_24_rt_179
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(1),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_1_rt_180
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(2),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_2_rt_181
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(3),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_3_rt_182
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(4),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_4_rt_183
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(5),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_5_rt_184
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(6),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_6_rt_185
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(7),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_7_rt_186
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(8),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_8_rt_187
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(9),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_9_rt_188
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(10),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_10_rt_189
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(11),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_11_rt_190
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(12),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_12_rt_191
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(13),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_13_rt_192
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(14),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_14_rt_193
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(15),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_15_rt_194
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_16_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(16),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_16_rt_195
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_17_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(17),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_17_rt_196
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_18_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(18),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_18_rt_197
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_19_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(19),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_19_rt_198
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_20_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(20),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_20_rt_199
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_21_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(21),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_21_rt_200
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_22_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(22),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_22_rt_201
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_23_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(23),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_23_rt_202
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_24_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(24),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_cy_24_rt_203
    );
  divider_1Hz_Mcount_temp_xor_25_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(25),
      O => divider_1Hz_Mcount_temp_xor_25_rt_204
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_25_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divider_1Hz_temp(25),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_xor_25_rt_205
    );
  divider_1Hz_temp_0_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result_0_1,
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_0_rstpot_207
    );
  divider_1Hz_temp_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_0_rstpot_207,
      Q => divider_1Hz_temp(0)
    );
  divider_1Hz_temp_1_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result_1_1_58,
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_1_rstpot_208
    );
  divider_1Hz_temp_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_1_rstpot_208,
      Q => divider_1Hz_temp(1)
    );
  divider_1Hz_temp_2_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result_2_1_59,
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_2_rstpot_209
    );
  divider_1Hz_temp_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_2_rstpot_209,
      Q => divider_1Hz_temp(2)
    );
  divider_1Hz_temp_3_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(3),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_3_rstpot_210
    );
  divider_1Hz_temp_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_3_rstpot_210,
      Q => divider_1Hz_temp(3)
    );
  divider_1Hz_temp_4_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(4),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_4_rstpot_211
    );
  divider_1Hz_temp_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_4_rstpot_211,
      Q => divider_1Hz_temp(4)
    );
  divider_1Hz_temp_5_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(5),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_5_rstpot_212
    );
  divider_1Hz_temp_5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_5_rstpot_212,
      Q => divider_1Hz_temp(5)
    );
  divider_1Hz_temp_6_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(6),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_6_rstpot_213
    );
  divider_1Hz_temp_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_6_rstpot_213,
      Q => divider_1Hz_temp(6)
    );
  divider_1Hz_temp_7_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(7),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_7_rstpot_214
    );
  divider_1Hz_temp_7 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_7_rstpot_214,
      Q => divider_1Hz_temp(7)
    );
  divider_1Hz_temp_8_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(8),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_8_rstpot_215
    );
  divider_1Hz_temp_8 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_8_rstpot_215,
      Q => divider_1Hz_temp(8)
    );
  divider_1Hz_temp_9_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(9),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_9_rstpot_216
    );
  divider_1Hz_temp_9 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_9_rstpot_216,
      Q => divider_1Hz_temp(9)
    );
  divider_1Hz_temp_10_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(10),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_10_rstpot_217
    );
  divider_1Hz_temp_10 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_10_rstpot_217,
      Q => divider_1Hz_temp(10)
    );
  divider_1Hz_temp_11_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(11),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_11_rstpot_218
    );
  divider_1Hz_temp_11 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_11_rstpot_218,
      Q => divider_1Hz_temp(11)
    );
  divider_1Hz_temp_12_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(12),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_12_rstpot_219
    );
  divider_1Hz_temp_12 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_12_rstpot_219,
      Q => divider_1Hz_temp(12)
    );
  divider_1Hz_temp_13_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(13),
      I1 => divider_1Hz_n0001_inv,
      O => divider_1Hz_temp_13_rstpot_220
    );
  divider_1Hz_temp_13 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_13_rstpot_220,
      Q => divider_1Hz_temp(13)
    );
  divider_1Hz_temp_14_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(14),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_14_rstpot_221
    );
  divider_1Hz_temp_14 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_14_rstpot_221,
      Q => divider_1Hz_temp(14)
    );
  divider_1Hz_temp_15_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(15),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_15_rstpot_222
    );
  divider_1Hz_temp_15 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_15_rstpot_222,
      Q => divider_1Hz_temp(15)
    );
  divider_1Hz_temp_16_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(16),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_16_rstpot_223
    );
  divider_1Hz_temp_16 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_16_rstpot_223,
      Q => divider_1Hz_temp(16)
    );
  divider_1Hz_temp_17_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(17),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_17_rstpot_224
    );
  divider_1Hz_temp_17 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_17_rstpot_224,
      Q => divider_1Hz_temp(17)
    );
  divider_1Hz_temp_18_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(18),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_18_rstpot_225
    );
  divider_1Hz_temp_18 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_18_rstpot_225,
      Q => divider_1Hz_temp(18)
    );
  divider_1Hz_temp_19_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(19),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_19_rstpot_226
    );
  divider_1Hz_temp_19 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_19_rstpot_226,
      Q => divider_1Hz_temp(19)
    );
  divider_1Hz_temp_20_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(20),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_20_rstpot_227
    );
  divider_1Hz_temp_20 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_20_rstpot_227,
      Q => divider_1Hz_temp(20)
    );
  divider_1Hz_temp_21_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(21),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_21_rstpot_228
    );
  divider_1Hz_temp_21 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_21_rstpot_228,
      Q => divider_1Hz_temp(21)
    );
  divider_1Hz_temp_22_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(22),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_22_rstpot_229
    );
  divider_1Hz_temp_22 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_22_rstpot_229,
      Q => divider_1Hz_temp(22)
    );
  divider_1Hz_temp_23_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(23),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_23_rstpot_230
    );
  divider_1Hz_temp_23 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_23_rstpot_230,
      Q => divider_1Hz_temp(23)
    );
  divider_1Hz_temp_24_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(24),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_24_rstpot_231
    );
  divider_1Hz_temp_24 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_24_rstpot_231,
      Q => divider_1Hz_temp(24)
    );
  divider_1Hz_temp_25_rstpot : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(25),
      I1 => divider_1Hz_n0001_inv4_238,
      O => divider_1Hz_temp_25_rstpot_232
    );
  divider_1Hz_temp_25 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => divider_1Hz_temp_25_rstpot_232,
      Q => divider_1Hz_temp(25)
    );
  divider_1Hz_n0001_inv3_SW0 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_17_Q,
      I1 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_16_Q,
      I2 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_18_Q,
      O => N6
    );
  divider_1Hz_n0001_inv4 : LUT6
    generic map(
      INIT => X"CCC888888C888888"
    )
    port map (
      I0 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_24_Q,
      I1 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_25_Q,
      I2 => divider_1Hz_n0001_inv2_142,
      I3 => N6,
      I4 => divider_1Hz_n0001_inv1_141,
      I5 => N7,
      O => divider_1Hz_n0001_inv
    );
  divider_1Hz_clk_t_dpot : LUT5
    generic map(
      INIT => X"565A555A"
    )
    port map (
      I0 => divider_1Hz_clk_t_1,
      I1 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_18_Q,
      I2 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_24_Q,
      I3 => divider_1Hz_n0001_inv1_141,
      I4 => N12,
      O => divider_1Hz_clk_t_dpot_206
    );
  divider_1Hz_n0001_inv4_rstpot_SW0_SW0 : LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
    port map (
      I0 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_7_Q,
      I1 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_8_Q,
      I2 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_9_Q,
      I3 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_10_Q,
      I4 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_11_Q,
      I5 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_12_Q,
      O => N14
    );
  divider_1Hz_n0001_inv4_rstpot_SW0 : LUT6
    generic map(
      INIT => X"007FFFFF00FFFFFF"
    )
    port map (
      I0 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_13_Q,
      I1 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_14_Q,
      I2 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_15_Q,
      I3 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_16_Q,
      I4 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_17_Q,
      I5 => N14,
      O => N12
    );
  divider_1Hz_n0001_inv3_SW1 : MUXF7
    port map (
      I0 => N16,
      I1 => display_0_OBUF_5,
      S => divider_1Hz_temp_25_GND_4_o_add_0_OUT_18_Q,
      O => N7
    );
  divider_1Hz_n0001_inv3_SW1_F : LUT6
    generic map(
      INIT => X"FFFF800000000000"
    )
    port map (
      I0 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_12_Q,
      I1 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_13_Q,
      I2 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_14_Q,
      I3 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_15_Q,
      I4 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_16_Q,
      I5 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_17_Q,
      O => N16
    );
  n0002_inv1 : LUT5
    generic map(
      INIT => X"33FFEA8A"
    )
    port map (
      I0 => current_state_FSM_FFd2_85,
      I1 => counter(0),
      I2 => current_state_FSM_FFd1_2,
      I3 => counter(1),
      I4 => counter(2),
      O => n0002_inv
    );
  current_state_FSM_FFd2_In1 : LUT5
    generic map(
      INIT => X"94468444"
    )
    port map (
      I0 => current_state_FSM_FFd2_85,
      I1 => counter(2),
      I2 => counter(0),
      I3 => counter(1),
      I4 => current_state_FSM_FFd1_2,
      O => current_state_FSM_FFd2_In
    );
  current_state_FSM_FFd1_In1 : LUT5
    generic map(
      INIT => X"C666666E"
    )
    port map (
      I0 => current_state_FSM_FFd2_85,
      I1 => current_state_FSM_FFd1_2,
      I2 => counter(0),
      I3 => counter(1),
      I4 => counter(2),
      O => current_state_FSM_FFd1_In
    );
  divider_1Hz_n0001_inv4_1 : LUT6
    generic map(
      INIT => X"CCC888888C888888"
    )
    port map (
      I0 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_24_Q,
      I1 => divider_1Hz_temp_25_GND_4_o_add_0_OUT_25_Q,
      I2 => divider_1Hz_n0001_inv2_142,
      I3 => N6,
      I4 => divider_1Hz_n0001_inv1_141,
      I5 => N7,
      O => divider_1Hz_n0001_inv4_238
    );
  clk_BUFGP : BUFGP
    port map (
      I => clk,
      O => clk_BUFGP_0
    );
  divider_1Hz_Mcount_temp_lut_0_INV_0 : INV
    port map (
      I => divider_1Hz_temp(0),
      O => divider_1Hz_Mcount_temp_lut(0)
    );
  divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_lut_0_INV_0 : INV
    port map (
      I => divider_1Hz_temp(0),
      O => divider_1Hz_Madd_temp_25_GND_4_o_add_0_OUT_lut_0_Q
    );
  current_state_n0026_1_1_INV_0 : INV
    port map (
      I => current_state_FSM_FFd2_85,
      O => segment_1_OBUF_4
    );
  Mcount_counter_xor_0_11_INV_0 : INV
    port map (
      I => counter(0),
      O => Result(0)
    );

end Structure;

