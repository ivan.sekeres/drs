library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sevenSegmentCNTRL is
    Port ( 
	 clk: in std_logic;
	 numbers: in std_logic_vector(15 downto 0);
	 display: out std_logic_vector(3 downto 0);
	 segment: out std_logic_vector(7 downto 0)
			  );
end sevenSegmentCNTRL;

architecture Behavioral of sevenSegmentCNTRL is

signal clk_div : std_logic;
signal num_sel : std_logic_vector(1 downto 0);

begin

	divider_1Hz	:	entity work.generic_divider generic map (50000000) port map (clk, clk_div);
	counter	:	entity work.binary_counter port map(clk_div, num_sel);
	
	process()
	begin
	
	end process;
	
end Behavioral;

